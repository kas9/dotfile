# start a terminal
# bindsym $mod+1 exec tilda
# bindsym $mod+shift+Return exec tilda
# kill focused window
#bindsym $mod+c kill
#bindsym $mod+z exec --no-startup-id rofi -modi drun -show drun -show-icons 
#bindsym $mod+z exec --no-startup-id /usr/local/share/rofi/colorful/launcher.sh

exec --no-startup-id volumeicon
exec --no-startup-id pa-applet
exec --no-startup-id udiskie
exec --no-startup-id /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
exec --no-startup-id nitrogen --restore; sleep 1; picom -b
exec --no-startup-id nm-applet
exec --no-startup-id xfce4-power-manager
exec --no-startup-id copyq
# exec --no-startup-id blueman-applet
# exec_always --no-startup-id sbxkb
#exec --no-startup-id start_conky_maia
#exec --no-startup-id start_conky_green