#################################################################
# KEYBOARD BINDINGS FOR ANY TWM
#################################################################

#################################################################
# SUPER + FUNCTION KEYS
#################################################################

# browser
super + F1
	chromium -no-default-browser-check

# file manager(GUI)
super + F2
	pcmanfm

# file manager
super + shift + F2(CLI)
    ranger

#Inkscape = vector graphics (alternative to Adobe Illustrator)
super + F3
    inkscape

#Gimp = image editing and photo retouching (alternative to Adobe Photoshop)
super + F4
    gimp

#Meld = Meld is a visual diff and merge tool. used for comparing 2 file or dir.
super + F5
    meld

#Mpv = (GUI video playwer written in C and very minimal and fast and it have great keyboard shortcuts)
super + F6 
    mpv --video-on-top


#Rofi Fullscreen
super + F11
    rofi -show drun -fullscreen

#Rofi
super + z
    rofi -modi drun -show drun -show-icons 
ctrl + alt + n
    hblock -S none -D none
#################################################################
# SUPER + ... KEYS
#################################################################


#Htop
super + h
    urxvt 'htop task manager' -e htop

#arcolinux-logout
super + x
    arcolinux-logout

#Pavucontrol
super + v
    pavucontrol

#Pragha
super + m
    pragha

#urxvt
super + Return
    urxvt

#Xkill
super + shift + n
    xkill


#################################################################
# SUPER + SHIFT KEYS
#################################################################

dmenu
	super + shift + y
		dmenu_run -i -nb '#191919' -nf '#fea63c' -sb '#fea63c' -sf '#191919' -fn 'NotoMonoRegular:bold:pixelsize=14'

reload sxhkd
	super + r
		pkill -USR1 -x sxhkd


#################################################################
# CONTROL + ALT KEYS
#################################################################


#arcolinux-tweak-tool
ctrl + alt + e
     arcolinux-tweak-tool

#Catfish
ctrl + alt + c
     catfish


#Nitrogen
ctrl + alt + i
     nitrogen


#Pamac-manager
super +  p
    pamac-manager

#Xfce4-settings-manager
ctrl + alt + m
     xfce4-settings-manager

#Pulse Audio Control
ctrl + alt + u
     pavucontrol

#Rofi theme selector
ctrl + alt + r
  rofi-theme-selector

#Spotify
ctrl + alt + s
   spotify

#urxvt
ctrl + alt + Return
    urxvt

#urxvt
ctrl + alt + t
    urxvt

#Vivaldi
ctrl + alt + v
    vivaldi-stable

#Xfce4-appfinder
ctrl + alt + a
    xfce4-appfinder


#Keyboard dependent
#urxvt
#ctrl + alt + KP_Enter
#     urxvt


#################################################################
# ALT + ... KEYS
#################################################################

#Wallpaper trash
alt + t
    variety -t

#Wallpaper next
alt + n
    variety -n

#Wallpaper previous
alt + p
    variety -p

#Wallpaper favorite
alt + f
    variety -f

#Wallpaper previous
alt + Left
    variety -p

#Wallpaper next
alt + Right
    variety -n

#Wallpaper toggle-pause
alt + Up
    variety --toggle-pause

#Wallpaper resume
alt + Down
    variety --resume

#Xfce4-appfinder
alt + F2
    xfce4-appfinder --collapsed

#Xfce4-appfinder
alt + F3
    xfce4-appfinder


#################################################################
#VARIETY KEYS WITH PYWAL
#################################################################

#Wallpaper trash
alt + shift + t
    variety -t && wal -i $(cat $HOME/.config/variety/wallpaper/wallpaper.jpg.txt)&

#Wallpaper next
alt + shift + n
    variety -n && wal -i $(cat $HOME/.config/variety/wallpaper/wallpaper.jpg.txt)&

#Wallpaper previous
alt + shift + p
    variety -p && wal -i $(cat $HOME/.config/variety/wallpaper/wallpaper.jpg.txt)&

#Wallpaper favorite
alt + shift + f
    variety -f && wal -i $(cat $HOME/.config/variety/wallpaper/wallpaper.jpg.txt)&

#Wallpaper update
alt + shift + u
    wal -i $(cat $HOME/.config/variety/wallpaper/wallpaper.jpg.txt)&

#################################################################
# CONTROL + SHIFT KEYS
#################################################################

#Xcfe4-TaskManager
ctrl + shift + Escape
    xfce4-taskmanager


#################################################################
#     SCREENSHOTS
#################################################################

#Scrot
Print
    scrot 'ArcoLinux-%Y-%m-%d-%s_screenshot_$wx$h.jpg' -e 'mv $f $$(xdg-user-dir PICTURES)'

#screeenshooter
ctrl + Print
     xfce4-screenshooter

#Gnome-Screenshot
ctrl + shift + Print
     gnome-screenshot -i


#################################################################
#     FUNCTION KEYS
#################################################################

#xfce4-terminal dropdown
F12
    xfce4-terminal --drop-down


#################################################################
#     MULTIMEDIA KEYS
#################################################################

#Raises volume
XF86AudioRaiseVolume
    amixer set Master 10%+

#Lowers volume
XF86AudioLowerVolume
    amixer set Master 10%-

#Mute
XF86AudioMute
    amixer -D pulse set Master 1+ toggle

#Playerctl works for Pragha, Spotify and others
#Delete the line for playerctl if you want to use mpc
#and replace it with the corresponding code
#mpc works for e.g.ncmpcpp
#mpc toggle
#mpc next
#mpc prev
#mpc stop

#PLAY
XF86AudioPlay
    playerctl play-pause

#Next
XF86AudioNext
    playerctl next

#previous
XF86AudioPrev
    playerctl previous

#Stop
XF86AudioStop
    playerctl stop

#Brightness up
XF86MonBrightnessUp
    xbacklight -inc 10

#Brightness down
XF86MonBrightnessDown
    xbacklight -dec 10

#########################
#        POLYBAR        #
#########################

#Hide polybar
super + y
    polybar-msg cmd toggle

#################################################################
#################################################################
##################   DESKTOP SPECIFIC    ########################
#################################################################
#################################################################

#################################################################
# CTRL + ALT KEYS
#################################################################

#Picom Toggle
ctrl + alt + o
    ~/.config/herbstluftwm/scripts/picom-toggle.sh

#################################################################
# SUPER + KEYS
#################################################################

# if you want to assign applications to specific tags or workspaces
# add a command behind the application to focus on that workspace if required
# index 0 corresponds to tag or workspace 1
# index 1 corresponds to tag or workspace	2
# example

# Vivaldi
#super + F1
#	vivaldi-stable & herbstclient use_index 0

#################################################################
# HLWM Terminate Keybindings
#################################################################

#close window/application
super + q
    herbstclient close

#close window/application
super + shift + q
    herbstclient close

#realod herbstluftwm
super + shift +r
    herbstclient reload

#Xkill
#super + shift + x
#    herbstclient quit




#################################################################
#################################################################
##################   AZERTY/QWERTY/ ... KEYBOARD#################
#################################################################
#################################################################
